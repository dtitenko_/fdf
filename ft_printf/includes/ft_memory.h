/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memory.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 18:11:14 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 18:29:19 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MEMORY_H
# define FT_MEMORY_H
# include <string.h>
# include <stdlib.h>

typedef unsigned char	t_uchar;

void	*ft_memcpy(void *s1, const void *s2, size_t n);
void	*ft_memalloc(size_t size);
void	ft_bzero(void *s, size_t n);

#endif

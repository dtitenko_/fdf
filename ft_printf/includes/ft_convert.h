/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 18:11:42 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 18:28:18 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_CONVERT_H
# define FT_CONVERT_H
# include <stdlib.h>

char				*ft_itoa(int n);
char				*ft_lltoa_base(long long value, int base);
int					ft_islower(int c);
int					ft_isupper(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);
int					get_size_for_base(long long n, int base);
char				get_base_char(int i);
char				*ft_ulltoa_base(unsigned long long value, int base);

char				*ft_ftoa(long double n, int dec, int decpoint);
char				*ft_ftoa_s(long double n, int perc, int decpoint);

int					ft_wctomb(char *str, wchar_t chr);
size_t				ft_wcstombs(char **dest, wchar_t *wstr);
size_t				ft_wcsntombs(char **dest, wchar_t *wstr, size_t len);
#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 18:33:42 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/30 18:41:34 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_memory.h"

char	*ft_strnew(size_t size)
{
	char	*s;

	if (NULL == (s = (char *)ft_memalloc(size + 1)))
		return (NULL);
	return (s);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pow.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 00:02:42 by dtitenko          #+#    #+#             */
/*   Updated: 2017/01/31 00:06:01 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

long long int	ft_llpow(long long int base, unsigned int exp)
{
	long long int t;

	if (exp == 0)
		return (1);
	t = ft_llpow(base, exp / 2);
	if (!(exp % 2))
		return (t * t);
	return (base * t * t);
}

long int		ft_lpow(long int base, unsigned int exp)
{
	return ((long int)ft_llpow(base, exp));
}

int				ft_pow(int base, unsigned int exp)
{
	return ((int)ft_llpow(base, exp));
}

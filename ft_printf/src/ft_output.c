/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_output.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/09 22:37:05 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:14:19 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_printf.h"
#include <unistd.h>

static char					g_spaces[] = "                                ";
static char					g_zeroes[] = "00000000000000000000000000000000";

int		ft_putn(const char *fmt, size_t n, t_format *x)
{
	if (0 < (long long)(n))
	{
		write(1, fmt, n);
		(*x).nchar += n;
		return (1);
	}
	else
		return (0);
}

void	ft_pad(const char *s, size_t n, t_format *x)
{
	size_t i;
	size_t j;

	if (0 < (long long)n)
	{
		j = n;
		while (0 < j)
		{
			i = (MAX_PAD < j) ? MAX_PAD : j;
			ft_putn(s, i, x);
			j -= i;
		}
	}
}

void	ft_output(t_format *px, char *ac)
{
	(*px).width -= (*px).n0 + (*px).nz0 +
			(*px).n1 + (*px).nz1 + (*px).n2 + (*px).nz2;
	if (!((*px).flags & FMI))
		ft_pad(g_spaces, (*px).width, px);
	ft_putn(ac, (*px).n0, px);
	ft_pad(g_zeroes, (*px).nz0, px);
	ft_putn((*px).s, (*px).n1, px);
	ft_pad(g_zeroes, (*px).nz1, px);
	ft_putn((*px).s + (*px).n1, (*px).n2, px);
	ft_pad(g_zeroes, (*px).nz2, px);
	if ((*px).flags & FMI)
		ft_pad(g_spaces, (*px).width, px);
}

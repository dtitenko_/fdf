/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_undefspec.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 02:58:17 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:14:45 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_printf.h"

void	ft_undefspec(t_format *px, char code, char *ac)
{
	int pad;

	(*px).s = &ac[(*px).n0];
	(*px).s[(*px).n1++] = code;
	if (((*px).flags & (FZE | FMI)) == FZE
		&& 0 < (pad = (*px).width - (*px).n0 - (*px).nz0 - (*px).n1))
		(*px).nz0 += pad;
}

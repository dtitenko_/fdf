/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_n_p_bouxX_DOU.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 02:58:17 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:14:15 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_printf.h"
#include "includes/ft_strings.h"
#include "includes/ft_convert.h"

void	ft_n(t_format *px, va_list pap, char code)
{
	if (code == 'n')
	{
		if ((*px).qual == 'h')
			((*px).qual1 == 'h') ? ((*va_arg(pap, char *)) = (*px).nchar)
								: ((*va_arg(pap, short *)) = (*px).nchar);
		else if ((*px).qual == 'l')
			((*px).qual1 == 'l') ? ((*va_arg(pap, long *)) = (*px).nchar)
								: ((*va_arg(pap, t_llong *)) = (*px).nchar);
		else if (ft_strchr("jz", (*px).qual))
			((*px).qual1 == 'j') ? ((*va_arg(pap, intmax_t *)) = (*px).nchar)
								: ((*va_arg(pap, ssize_t *)) = (*px).nchar);
		else
			(*va_arg(pap, int *)) = (*px).nchar;
	}
}

void	ft_p(t_format *px, va_list pap, char code, char *ac)
{
	if (code == 'p')
	{
		ac[(*px).n0++] = '0';
		ac[(*px).n0++] = 'x';
		(*px).v.li = (long)va_arg(pap, void *);
		(*px).s = &ac[(*px).n0];
		ft_litob(px, 'x');
	}
}

void	ft_boux(t_format *px, va_list pap, char code, char *ac)
{
	if (ft_strchr("bouxX", code))
	{
		if ((*px).qual == 'l')
			(*px).v.li = ((*px).qual1 == 'l') ?
						va_arg(pap, t_ullong) : va_arg(pap, t_ulong);
		else if ((*px).qual == 'h')
			(*px).v.li = ((*px).qual1 == 'h') ?
						(unsigned char)va_arg(pap, int) :
						(unsigned short)va_arg(pap, int);
		else if ((*px).qual == 'j' || (*px).qual == 'z')
			(*px).v.li = ((*px).qual1 == 'j') ?
						va_arg(pap, uintmax_t) : va_arg(pap, size_t);
		else
			(*px).v.li = (unsigned int)va_arg(pap, int);
		if (((*px).flags & FNO) && code != 'u' && (((code == 'o' &&
			!((*px).v.li) && ((*px).prec == 0)) || (*px).v.li)))
		{
			ac[(*px).n0++] = '0';
			(*px).prec -= (code == 'o' && ((*px).v.li)) ? 1 : 0;
			if (ft_strchr("bxX", code))
				ac[(*px).n0++] = code;
		}
		(*px).s = &ac[(*px).n0];
		ft_litob(px, code);
	}
}

void	ft_ldou(t_format *px, va_list pap, char code, char *ac)
{
	if (ft_strchr("DOU", code))
	{
		if ((*px).qual == 'l')
			(*px).qual1 = 'l';
		(*px).qual = 'l';
		code = ft_tolower(code);
		ft_di(px, pap, code, ac);
		ft_boux(px, pap, code, ac);
	}
}

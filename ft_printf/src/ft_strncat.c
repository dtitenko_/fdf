/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 19:00:00 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/26 19:24:07 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_strings.h"

char	*ft_strncat(char *dst, const char *src, size_t n)
{
	size_t	destlen;
	size_t	srclen;

	destlen = ft_strlen(dst);
	srclen = ft_strlen(src);
	if (srclen < n)
		ft_strcpy((dst + destlen), src);
	else
	{
		ft_strncpy((dst + destlen), src, n);
		*(dst + destlen + n) = 0;
	}
	return (dst);
}

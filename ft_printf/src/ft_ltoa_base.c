/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ltoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 11:52:43 by exam              #+#    #+#             */
/*   Updated: 2016/12/29 14:59:56 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		get_size_for_base(long long n, int base)
{
	int					i;
	unsigned long long	tmp;

	i = 0;
	if (!n)
		return (1);
	if (n < 0 && base == 10)
		i++;
	tmp = n;
	while (tmp)
	{
		i++;
		tmp /= base;
	}
	return (i);
}

char	get_base_char(int i)
{
	if (i > 36)
		return (0);
	if (i < 10)
		return ('0' + i);
	else
		return ('a' + (i - 10));
}

char	*ft_lltoa_base(long long value, int base)
{
	char				*res;
	int					len;
	unsigned long long	tmp;

	len = get_size_for_base(value, base);
	tmp = value;
	if (!((res = (char *)malloc(len + 1)) && base >= 2 && base <= 36))
		return (char *)0;
	if (value < 0)
	{
		if (base == 10)
			res[0] = '-';
	}
	res[len] = '\0';
	value == 0 ? res[0] = '0' : 0;
	while (tmp)
	{
		--len;
		res[len] = get_base_char(tmp % base);
		tmp /= base;
	}
	return (res);
}

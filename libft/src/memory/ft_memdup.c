/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 17:41:50 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 17:49:55 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_memory.h>
#include <stdlib.h>

void	*ft_memdup(const void *oldmem, size_t size)
{
	void	*newmem;

	if (oldmem)
	{
		newmem = malloc(size);
		if (newmem)
			ft_memmove(newmem, oldmem, size);
		return (newmem);
	}
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ftoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/05 20:13:35 by dtitenko          #+#    #+#             */
/*   Updated: 2017/02/05 20:15:23 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*parse_int_part(char **res, long double *n, int *dec, size_t *len)
{
	long long int	intp;
	char			*swap;

	intp = (long long int)*n;
	swap = ft_lltoa_base(ft_llabs(intp), 10);
	if (!(*res))
	{
		(*len) = ft_strlen(swap) + *dec + 1;
		(*len) += (*n < 0) ? 1 : 0;
	}
	if (!(*res) && !((*res) = ft_strnew(*len)))
		return (NULL);
	if ((*n) < 0)
		ft_strcat(*res, "-");
	(*n) -= intp;
	(*n) = ft_fabs(*n);
	ft_strcat(*res, swap);
	ft_strdel(&swap);
	return (*res);
}

char	*ft_ftoa(long double n, int dec, int decpoint)
{
	char		*res;
	size_t		len;
	size_t		i;

	res = NULL;
	parse_int_part(&res, &n, &dec, &len);
	if (decpoint || dec > 0)
		ft_strcat(res, ".");
	if (dec < 1)
		return (res);
	i = ft_strlen(res);
	while (dec--)
	{
		n -= (long double)((int)(n));
		n *= 10.0;
		res[i++] = (char)('0' + (int)n);
	}
	len -= ft_strlen(res);
	while (len--)
		ft_strcat(res, "0");
	return (res);
}

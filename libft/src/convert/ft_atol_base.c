/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atol_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/28 16:43:39 by dtitenko          #+#    #+#             */
/*   Updated: 2017/01/28 16:44:13 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long	ft_atol_base(char *str, int base)
{
	long n;
	char sign;
	char *sd;
	char *digits;

	digits = ft_strdup("0123456789abcdefghijklmnopqrstuvwxyz");
	n = 0;
	if (!str || !*str || base < 2 || base > 36)
		return (n);
	str += ft_count_whitespaces(str);
	sign = (base == 10 && *str == '-') ? -1 : 1;
	(base == 10 && (*str == '-' || *str == '+')) ? str++ : 0;
	if (base == 16 && *str == '0' && ft_tolower(*(str + 1)) == 'x')
		str += 2;
	while (*str && (sd = (char *)ft_memchr(digits, ft_tolower(*str++), base)))
		n = n * base + (sd - digits);
	return (sign * n);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_blend_add.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 17:45:33 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 17:46:14 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_colors.h>

t_color				color_blend_add(t_color a, t_color b)
{
	t_color			c;
	int				red;
	int				green;
	int				blue;

	red = (int)a.argb.r * (int)a.argb.a + (int)b.argb.r * (int)b.argb.a;
	green = (int)a.argb.g * (int)a.argb.a + (int)b.argb.g * (int)b.argb.a;
	blue = (int)a.argb.b * (int)a.argb.a + (int)b.argb.b * (int)b.argb.a;
	if ((int)a.argb.a + (int)b.argb.a < 255)
		c.argb.a = (unsigned char)((int)a.argb.a + (int)b.argb.a);
	else
		c.argb.a = 255;
	if (red < 255 * 255)
		c.argb.r = (unsigned char)(red / 255);
	else
		c.argb.r = 255;
	if (green < 255 * 255)
		c.argb.g = (unsigned char)(green / 255);
	else
		c.argb.g = 255;
	if (blue < 255 * 255)
		c.argb.b = (unsigned char)(blue / 255);
	else
		c.argb.b = 255;
	return (c);
}

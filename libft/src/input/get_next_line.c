/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 19:58:18 by dtitenko          #+#    #+#             */
/*   Updated: 2017/01/18 20:48:11 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"
#include <fcntl.h>
#include <unistd.h>

t_read			*newtread(int fd)
{
	t_read	*el;

	if (!(el = (t_read *)malloc(sizeof(t_read *))))
		return (NULL);
	el->buf = ft_strnew(BUFF_SIZE);
	el->fd = fd;
	return (el);
}

char			**apply_swap(char **line, char *buf)
{
	char *swap;

	if (!(line))
		return (NULL);
	swap = ft_strjoin(*line, buf);
	ft_strdel(line);
	*line = ft_strdup(swap);
	ft_strdel(&swap);
	ft_strclr(buf);
	return (line);
}

t_read			*find_buff(t_list **lst, int fd)
{
	t_list	*curr;
	t_read	*tread;

	if (!lst)
		return (NULL);
	curr = *lst;
	while (curr)
	{
		if (((t_read *)curr->content)->fd == fd)
			return ((t_read *)curr->content);
		curr = curr->next;
	}
	if (!(tread = newtread(fd)))
		return (NULL);
	curr = ft_lstnew(tread, sizeof(tread));
	free(tread);
	tread = NULL;
	((t_read *)curr->content)->fd = fd;
	ft_lstadd(lst, curr);
	return ((t_read *)curr->content);
}

int				ft_read_line(t_read **tread, char **line)
{
	char	*eol;
	int		ret;
	char	*swap;

	ret = 1;
	swap = NULL;
	while (!(eol = ft_strchr((*tread)->buf, '\n')) && ret > 0)
	{
		apply_swap(line, (*tread)->buf);
		if ((ret = read((*tread)->fd, (*tread)->buf, BUFF_SIZE)) < 1
			&& !(**line))
			return (ret);
	}
	if (eol && !(swap = ft_strsub((*tread)->buf, 0, eol - (*tread)->buf)))
		return (-1);
	if (eol && !(line = apply_swap(line, swap)))
		return (-1);
	ft_strdel(&swap);
	swap = eol ? ft_strdup(eol + 1) : ft_strnew(0);
	ft_strclr((*tread)->buf);
	ft_strcat((*tread)->buf, swap);
	ft_strdel(&swap);
	return (1);
}

int				get_next_line(int fd, char **line)
{
	static t_list	*lst;
	t_read			*tread;
	int				ret;

	if (!line)
		return (-1);
	*line = ft_strnew(0);
	if (!(*line))
		return (-1);
	if (!(tread = find_buff(&lst, fd)))
	{
		ft_strdel(line);
		return (-1);
	}
	if ((ret = ft_read_line(&tread, line)) < 1)
		ft_strdel(line);
	return (ret);
}

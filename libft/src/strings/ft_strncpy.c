/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 18:37:32 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/01 19:30:31 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dest, const char *src, size_t len)
{
	char *s;

	s = dest;
	while (len && *src)
	{
		*s++ = *src++;
		--len;
	}
	while (len--)
		*s++ = '\0';
	return (dest);
}

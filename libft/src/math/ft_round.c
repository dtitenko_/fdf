/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_round.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/07 19:50:20 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:16:45 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long double	ft_roundl(long double num, unsigned int dec)
{
	long double exp;
	long double fract;

	(void)num;
	exp = ft_powf(10, dec + 1);
	num *= exp;
	fract = num - (long double)(long long)num;
	num -= fract;
	if ((long double)((long long)num % 10) >= 5.0)
		num += 10;
	num /= exp;
	return (num);
}

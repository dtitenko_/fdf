# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/12/26 18:45:59 by dtitenko          #+#    #+#              #
#    Updated: 2017/04/09 23:13:33 by dtitenko         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf


LIBNAME = ft
LIBDIR = ./libft
LIBINCLUDEFOLDERS = ./libft/includes

PRINTFNAME = ftprintf
PRINTFDIR = ./ft_printf
PRINTFINCUDES = ./ft_printf/includes

MLXNAME = mlx
MLXDIR = ./minilibx_x11
MLXINCLUDES = ./minilibx_x11

X11INCLUDES = /opt/X11/include

INCLUDES = ./includes

SOURCES_FOLDER = src/
OBJECTS_FOLDER = obj/

SOURCES = \
		affine.c                helpers.c               linalg.c                read.c \
		dda.c                   hooks.c                 linalg_init.c           scale_handler.c \
		draw.c                  image.c                 main.c                  usage.c \
		free_fdf.c              init_affine_matrixes.c  map.c                   vertex.c \
		handler.c               init_fdf.c              projections.c

OBJECTS = $(SOURCES:.c=.o)
OBJECTS := $(subst /,__,$(OBJECTS))
OBJECTS := $(addprefix $(OBJECTS_FOLDER), $(OBJECTS))
SOURCES := $(addprefix $(SOURCES_FOLDER), $(SOURCES))

CC = gcc
AR = ar
CFLAGS = -Wall -Werror -Wextra


# Colors

NO_COLOR =		\033[0;00m
OK_COLOR =		\033[38;5;02m
ERROR_COLOR =	\033[38;5;01m
WARN_COLOR =	\033[38;5;03m
SILENT_COLOR =	\033[38;5;04m


# Basic Rules

.PHONY: all re clean fclean tests

all: $(NAME)

$(NAME): $(OBJECTS) $(MLXNAME) $(LIBNAME) $(PRINTFNAME)
	$(CC) -g $(CFLAGS) \
	-I$(INCLUDES) -I$(X11INCLUDES) \
	-I$(PRINTFINCUDES) -I$(LIBINCLUDEFOLDERS)  -I$(MLXINCLUDES) \
	-L$(PRINTFDIR) -l$(PRINTFNAME) \
	-L$(LIBDIR) -l$(LIBNAME) \
	-L/opt/X11/lib -lX11 -lXext \
     -o $(NAME) minilibx_x11/libmlx.a $(OBJECTS)

$(OBJECTS_FOLDER)%.o: $(SOURCES_FOLDER)%.c
	@mkdir -p $(OBJECTS_FOLDER)
	$(CC) -c $(CFLAGS)  -I$(INCLUDES) -I$(X11INCLUDES)\
	 -I$(PRINTFINCUDES) -I$(LIBINCLUDEFOLDERS)  -I$(MLXINCLUDES)\
	 -o $@ $<

$(MLXNAME):
	make -C $(MLXDIR) all

$(LIBNAME):
	make -C $(LIBDIR) all

$(PRINTFNAME):
	make -C $(PRINTFDIR) all


clean:
	rm -f $(OBJECTS)
	rm -rf $(OBJECTS_FOLDER)
	@printf "$(SILENT_COLOR)$(NAME) : Cleaned Objects libftprintf$(NO_COLOR)\n"

fclean: clean
	make -C $(MLXDIR) clean
	make -C $(LIBDIR) fclean
	make -C $(PRINTFDIR) fclean
	rm -f ./basic_test
	rm -f $(NAME)
	@printf "$(SILENT_COLOR)$(NAME) : Cleaned Library libftprintf$(NO_COLOR)\n"

re: fclean all
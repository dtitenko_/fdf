/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helpers.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 15:08:50 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:53:02 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <libft.h>
#include "fdf.h"
#include "linalg.h"

void	ft_die(int argc, char **argv, int code, t_mlx **mlx)
{
	ft_mlxdel(mlx);
	if (code == ENODEV)
		ft_printf("{r}%s : invalid number of arguments : %d\n",
				argv[0], argc - 1);
	if (code == ENOENT && argc > 1)
		ft_printf("{r}%s : cannot access %s : no such file or directory{eoc}\n",
				argv[0], argv[1]);
	if (code == EIO && argc > 1)
		ft_printf("{r}%s : %s : invalid file{eoc}\n", argv[0], argv[1]);
	if (code == ENOMEM)
		ft_printf("{r}%s : error : cannot allocate memory{eoc}\n", argv[0]);
	exit(code);
}

void	ft_linesdel(char ***lines)
{
	char **tmp;

	if (!lines || !(*lines))
		return ;
	tmp = *lines;
	while (**lines)
	{
		ft_strdel(&(**lines));
		(*lines)++;
	}
	ft_memdel((void **)(tmp));
	(*lines) = NULL;
}

void	move_center(t_map *map, t_mlx *mlx)
{
	mlx->trans->sx = mlx->img_p->width / map->width / 2;
	mlx->trans->sy = mlx->img_p->height / map->height / 2;
	mlx->trans->sz = 10;
}

int		update(t_mlx *mlx)
{
	ft_bzero(mlx->img_p->data,
			mlx->img_p->width * mlx->img_p->height * mlx->img_p->bpp / 8);
	mlx_clear_window(mlx->mlx_p, mlx->win_p);
	if (mlx->flags & POINTSMODE)
		draw_points(mlx);
	else
		draw(mlx);
	if (mlx->flags & SHOWAXIS)
		draw_axis(mlx);
	mlx_put_image_to_window(mlx->mlx_p, mlx->win_p, mlx->img_p, 0, 0);
	mlx->flags &= ~POINTSMODE;
	mlx->flags |= LINESMODE;
	if (mlx->flags & SHOWHELP)
		draw_usage(mlx);
	if (mlx->flags & SHOWINFO)
		draw_info(mlx);
	return (0);
}

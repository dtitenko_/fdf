/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   affine.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 15:08:50 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:52:32 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "linalg.h"
#include "affine.h"

t_vertex3d	translate(t_vertex3d v, double x, double y, double z)
{
	t_matrix4x4 m;

	identity(&m);
	translate_matrix(&m, x, y, z);
	v = dot_mxv(v, m);
	return (v);
}

t_vertex3d	scale(t_vertex3d v, double sx, double sy, double sz)
{
	v.x *= (sx != 0) ? sx : 1;
	v.y *= (sy != 0) ? sy : 1;
	v.z *= (sz != 0) ? sz : 1;
	return (v);
}

t_vertex3d	rotate(t_vertex3d v, double rx, double ry, double rz)
{
	t_matrix4x4 m;

	identity(&m);
	rotatex_matrix(&m, rx);
	v = dot_mxv(v, m);
	identity(&m);
	rotatey_matrix(&m, ry);
	v = dot_mxv(v, m);
	identity(&m);
	rotatez_matrix(&m, rz);
	v = dot_mxv(v, m);
	return (v);
}

t_map		*transform_map(t_map *map, t_trans *trans)
{
	int			i;
	int			j;
	t_vertex3d	first;

	i = -1;
	first = map->vrtxs[0][0];
	while (++i < map->height)
	{
		j = -1;
		while (++j < map->width)
		{
			map->vrtxs[i][j] = translate(map->vrtxs[i][j],
								-first.x / 2.0, -first.y / 2.0, -first.z);
			map->vrtxs[i][j] = scale(map->vrtxs[i][j],
								trans->sx, trans->sy, trans->sz);
			map->vrtxs[i][j] = translate(map->vrtxs[i][j],
								first.x / 2.0, first.y / 2.0, first.z);
			map->vrtxs[i][j] = translate(map->vrtxs[i][j],
								trans->tx, trans->ty, trans->tz);
			map->vrtxs[i][j] = rotate(map->vrtxs[i][j],
								trans->rotx, trans->roty, trans->rotz);
		}
	}
	ft_transzero(trans);
	return (map);
}

void		ft_transzero(t_trans *trans)
{
	trans->tx = 0;
	trans->ty = 0;
	trans->tz = 0;
	trans->sx = 1;
	trans->sy = 1;
	trans->sz = 1;
	trans->rotx = 0;
	trans->roty = 0;
	trans->rotz = 0;
}

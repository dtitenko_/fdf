/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 15:22:40 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:52:51 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_vertex3d	*init_axis(void)
{
	t_vertex3d	*axis;

	if (!(axis = malloc(4 * sizeof(t_vertex3d))))
		return (NULL);
	axis[0].y = 0;
	axis[0].z = 0;
	axis[0].x = 100;
	axis[0].color.color = 0xFF0000;
	axis[1].x = 0;
	axis[1].y = 100;
	axis[1].z = 0;
	axis[1].color.color = 0xFF00;
	axis[2].x = 0;
	axis[2].y = 0;
	axis[2].z = 100;
	axis[2].color.color = 0xFF;
	axis[3].x = 0;
	axis[3].y = 0;
	axis[3].z = 0;
	axis[3].color.color = 0x00FFFFFF;
	return (axis);
}

void		draw_axis(t_mlx *mlx)
{
	t_vertex3d	*axis;
	int			i;

	if (!(axis = init_axis()))
		return ;
	i = 4;
	while (i--)
	{
		axis[i] = rotate(axis[i], mlx->trans->rotx, mlx->trans->roty,
						mlx->trans->rotz);
		axis[i] = translate(axis[i], mlx->img_p->width - 100,
							100, 0);
		axis[i] = mlx->proj_func(axis[i]);
		if (i < 3)
		{
			axis[3].color.color = axis[i].color.color;
			ft_dda(axis[3], axis[i], mlx);
		}
	}
	free(axis);
	axis = NULL;
}

void		draw_points(t_mlx *mlx)
{
	int			i;
	int			j;
	t_vertex3d	v;

	i = -1;
	while (++i < mlx->map->height)
	{
		j = -1;
		while (++j < mlx->map->width)
		{
			v = mlx->map->vrtxs[i][j];
			v = transform_vertex(v, mlx->trans);
			v = translate(v,
				mlx->img_p->width / 2.0, mlx->img_p->height / 2.0, 0.0);
			v = mlx->proj_func(v);
			img_put_pixel(mlx->img_p, &v, v.color.color);
		}
	}
}

void		draw_quad(t_vertex3d *vrtxs, t_mlx *mlx)
{
	int	i;

	i = 4;
	while (i--)
	{
		vrtxs[i] = transform_vertex(vrtxs[i], mlx->trans);
		vrtxs[i] = translate(vrtxs[i],
					mlx->img_p->width / 2.0, mlx->img_p->height / 2.0, 0.0);
		vrtxs[i] = mlx->proj_func(vrtxs[i]);
	}
	ft_dda(vrtxs[0], vrtxs[1], mlx);
	ft_dda(vrtxs[0], vrtxs[2], mlx);
	if (mlx->flags & DIAGLINES)
		ft_dda(vrtxs[0], vrtxs[3], mlx);
	if (mlx->flags & QUADSMODE)
	{
		ft_dda(vrtxs[1], vrtxs[3], mlx);
		ft_dda(vrtxs[2], vrtxs[3], mlx);
	}
}

void		draw(t_mlx *mlx)
{
	int			i;
	int			j;
	t_vertex3d	*vrtxs;

	if (!(vrtxs = (t_vertex3d *)malloc(sizeof(t_vertex3d) * 4)))
		return ;
	i = -1;
	while (++i < mlx->map->height - 1)
	{
		j = -1;
		while (++j < mlx->map->width - 1)
		{
			vrtxs[0] = mlx->map->vrtxs[i][j];
			vrtxs[1] = mlx->map->vrtxs[i][j + 1];
			vrtxs[2] = mlx->map->vrtxs[i + 1][j];
			vrtxs[3] = mlx->map->vrtxs[i + 1][j + 1];
			draw_quad(vrtxs, mlx);
		}
	}
}

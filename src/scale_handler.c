/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scale_handler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 15:24:20 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:53:44 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <X11/keysym.h>
#include "fdf.h"

void	ft_scale(t_trans *trans, double sx, double sy, double sz)
{
	if (trans
		&& (trans->sx + sx > 0)
		&& (trans->sy + sy > 0)
		&& (trans->sz + sz > 0))
	{
		trans->sx += sx;
		trans->sy += sy;
		trans->sz += sz;
	}
}

void	ft_scale_handler(t_mlx *mlx, int keycode)
{
	double	step;

	step = 1;
	if (XK_KP_Add == keycode)
		ft_scale(mlx->trans, step, step, step);
	else if (XK_KP_Subtract == keycode)
		ft_scale(mlx->trans, -step, -step, -step);
	if (mlx->keymodif & CTRLMOD)
		step = -step;
	if (XK_z == keycode)
		ft_scale(mlx->trans, 0.0, 0.0, step);
	else if (XK_x == keycode)
		ft_scale(mlx->trans, step, 0.0, 0.0);
	else if (XK_c == keycode)
		ft_scale(mlx->trans, 0.0, step, 0.0);
	if (keycode == XK_KP_Add || keycode == XK_KP_Subtract
		|| XK_z == keycode || XK_x == keycode || XK_c == keycode)
		mlx->flags |= POINTSMODE;
}

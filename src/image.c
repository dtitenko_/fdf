/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   image.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 16:31:21 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:53:08 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	img_put_pixel(t_img *image, t_vertex3d *v, int color)
{
	if (v->x < 0 || v->x >= image->width || v->y < 0 || v->y >= image->height)
		return ;
	*(int *)(image->data +
			((int)v->x + (int)v->y * image->width) * image->bpp / 8) =
			color;
}

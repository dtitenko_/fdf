/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_affine_matrixes.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 14:04:02 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:53:12 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "linalg.h"
#include "math.h"

t_matrix4x4	*translate_matrix(t_matrix4x4 *m, double dx, double dy, double dz)
{
	(*m)[0][3] = dx;
	(*m)[1][3] = dy;
	(*m)[2][3] = dz;
	return (m);
}

t_matrix4x4	*rotatex_matrix(t_matrix4x4 *m, double teta)
{
	teta = (teta * M_PI / 180);
	(*m)[1][1] = cos(teta);
	(*m)[2][2] = cos(teta);
	(*m)[1][2] = -sin(teta);
	(*m)[2][1] = sin(teta);
	return (m);
}

t_matrix4x4	*rotatey_matrix(t_matrix4x4 *m, double teta)
{
	teta = (teta * M_PI / 180);
	(*m)[0][0] = cos(teta);
	(*m)[2][2] = cos(teta);
	(*m)[0][2] = sin(teta);
	(*m)[2][0] = -sin(teta);
	return (m);
}

t_matrix4x4	*rotatez_matrix(t_matrix4x4 *m, double teta)
{
	teta = (teta * M_PI / 180);
	(*m)[0][0] = cos(teta);
	(*m)[0][1] = -sin(teta);
	(*m)[1][0] = sin(teta);
	(*m)[1][1] = cos(teta);
	return (m);
}

t_matrix4x4	*scale_matrix(t_matrix4x4 *m, double sx, double sy, double sz)
{
	(*m)[0][0] = (sx != 0) ? sx : 1;
	(*m)[1][1] = (sy != 0) ? sy : 1;
	(*m)[2][2] = (sz != 0) ? sz : 1;
	return (m);
}

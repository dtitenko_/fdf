/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handler.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 15:08:50 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:52:58 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <math.h>

double	normalize_angle(double angl)
{
	angl = fmod(angl, 360.0);
	if (angl < 0.0)
		angl += 360.0;
	return (angl);
}

void	ft_rotate_handler(t_mlx *mlx, int keycode)
{
	int ang;

	ang = 5;
	if (XK_w == keycode)
		mlx->trans->rotx += ang;
	else if (XK_s == keycode)
		mlx->trans->rotx -= ang;
	else if (XK_a == keycode)
		mlx->trans->roty -= ang;
	else if (XK_d == keycode)
		mlx->trans->roty += ang;
	else if (XK_q == keycode)
		mlx->trans->rotz -= ang;
	else if (XK_e == keycode)
		mlx->trans->rotz += ang;
	mlx->trans->rotx = normalize_angle(mlx->trans->rotx);
	mlx->trans->roty = normalize_angle(mlx->trans->roty);
	mlx->trans->rotz = normalize_angle(mlx->trans->rotz);
	if (keycode == XK_w || keycode == XK_s
		|| XK_a == keycode || XK_d == keycode
		|| XK_q == keycode || XK_e == keycode)
		mlx->flags |= POINTSMODE;
}

void	ft_trans_handler(t_mlx *mlx, int keycode)
{
	double	step;

	step = 5;
	if (XK_Right == keycode)
		mlx->trans->tx += step;
	else if (XK_Left == keycode)
		mlx->trans->tx -= step;
	else if (XK_Up == keycode)
		mlx->trans->ty -= step;
	else if (XK_Down == keycode)
		mlx->trans->ty += step;
	if (keycode == XK_Right || keycode == XK_Left
		|| XK_Up == keycode || XK_Down == keycode)
		mlx->flags |= POINTSMODE;
}

void	ft_modification_handlers(t_mlx *mlx, int keycode)
{
	if (XK_Alt_R == keycode || XK_Alt_L == keycode)
		mlx->keymodif |= ALTMOD;
	else if (XK_Shift_R == keycode || XK_Shift_L == keycode)
		mlx->keymodif |= SHFTMOD;
	else if (XK_Control_L == keycode || XK_Control_R == keycode)
		mlx->keymodif |= CTRLMOD;
}

void	ft_draw_handlers(t_mlx *mlx, int keycode)
{
	if (XK_r == keycode)
	{
		ft_transzero(mlx->trans);
		move_center(mlx->map, mlx);
	}
	else if (XK_p == keycode)
		mlx->proj_func = (mlx->proj_func == ortho) ? cabinet : ortho;
	else if (XK_l == keycode)
		mlx->flags = (mlx->flags & DIAGLINES) ? mlx->flags & (~DIAGLINES) :
					mlx->flags | DIAGLINES;
	else if (XK_f == keycode)
		mlx->flags = (mlx->flags & QUADSMODE) ? mlx->flags & (~QUADSMODE) :
					mlx->flags | QUADSMODE;
	else if (XK_F1 == keycode)
		mlx->flags = (mlx->flags & SHOWHELP) ?
					mlx->flags & (~SHOWHELP) : mlx->flags | SHOWHELP;
	else if (XK_F2 == keycode)
		mlx->flags = (mlx->flags & SHOWINFO) ?
					mlx->flags & (~SHOWINFO) : mlx->flags | SHOWINFO;
	else if (XK_F3 == keycode)
		mlx->flags = (mlx->flags & SHOWAXIS) ?
					mlx->flags & (~SHOWAXIS) : mlx->flags | SHOWAXIS;
}

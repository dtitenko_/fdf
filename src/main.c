/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/16 15:56:25 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/16 15:56:52 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "mlx.h"
#include "fdf.h"

void	ft_init_hooks(t_mlx *mlx)
{
	mlx_key_hook(mlx->win_p, keyrelease_hooks, mlx);
	mlx_hook(mlx->win_p, DestroyNotify, StructureNotifyMask, exit_x, mlx);
	mlx_hook(mlx->win_p, KeyPress, KeyPressMask, keypress_hooks, mlx);
	mlx_expose_hook(mlx->win_p, update, mlx);
}

t_mlx	*ft_init_mlx(void)
{
	t_mlx *mlx;

	if (!(mlx = (t_mlx *)malloc(sizeof(t_mlx))))
		exit(-1);
	mlx->img_p = NULL;
	mlx->win_p = NULL;
	mlx->keymodif = 0;
	mlx->flags = LINESMODE | QUADSMODE | SHOWHELP | SHOWINFO | SHOWAXIS;
	if (!(mlx->mlx_p = mlx_init()))
		return (NULL);
	mlx->win_p = mlx_new_window(mlx->mlx_p, WIN_W, WIN_H, "fdf");
	mlx->img_p = mlx_new_image(mlx->mlx_p, WIN_W, WIN_H);
	mlx->proj_func = ortho;
	mlx->srcmap = NULL;
	mlx->map = NULL;
	mlx_do_key_autorepeaton(mlx->mlx_p);
	if (!(mlx->trans = init_trans()))
		return (NULL);
	ft_transzero(mlx->trans);
	ft_init_hooks(mlx);
	return (mlx);
}

int		main(int argc, char **argv)
{
	t_mlx	*mlx;
	int		ret;

	(void)argv;
	(void)argc;
	mlx = ft_init_mlx();
	if (argc == 2)
	{
		if (1 != (ret = ft_read(argv[1], mlx)))
			ft_die(argc, argv, ret, &mlx);
	}
	else
		ft_die(argc, argv, ENODEV, &mlx);
	mlx_loop(mlx->mlx_p);
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 15:08:50 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:53:40 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "fdf.h"
#include "fcntl.h"

int		ft_count_strings(char *line)
{
	char	**lines;
	char	**tmp;
	int		w;

	if (!(lines = ft_strsplit(line, ' ')))
		return (0);
	tmp = lines;
	w = 0;
	while (*lines++)
		w++;
	ft_linesdel(&tmp);
	return (w);
}

int		ft_get_params_map(int fd, int *width, int *length)
{
	char	*line;
	int		ret;
	int		flag;
	int		w;

	flag = 1;
	(*length) = 0;
	(*width) = 0;
	while ((ret = get_next_line(fd, &line)))
	{
		if (ret == -1)
			return (ret);
		w = ft_count_strings(line);
		if (flag)
		{
			(*width) = w;
			flag = 0;
		}
		if (w != (*width))
			return (0);
		(*length)++;
		ft_strdel(&line);
	}
	return (1);
}

void	put_vertex(t_map *map, char *line, int i, int j)
{
	map->vrtxs[i][j].z = ft_atoi(line);
	map->vrtxs[i][j].x = j;
	map->vrtxs[i][j].y = i;
	map->vrtxs[i][j].color.color = (ft_strchr(line, ',')) ?
			(int)ft_atol_base(ft_strchr(line, ',') + 1, 16) : 0xFFFFFF;
}

int		ft_parse_file(int fd, t_mlx *mlx)
{
	int		i;
	int		j;
	char	*line;
	char	**lines;
	char	**tmp;

	i = -1;
	while (0 < (get_next_line(fd, &line)) && ++i < mlx->srcmap->height)
	{
		lines = ft_strsplit(line, ' ');
		ft_strdel(&line);
		tmp = lines;
		j = -1;
		while (*lines && ++j < mlx->srcmap->width)
		{
			put_vertex(mlx->srcmap, *lines, i, j);
			lines++;
		}
		ft_linesdel(&tmp);
		if (j + 1 != mlx->srcmap->width)
			return (0);
	}
	if (i + 1 != mlx->srcmap->height)
		return (0);
	return (1);
}

int		ft_read(char *filename, t_mlx *mlx)
{
	int			fd;
	int			len;
	int			width;
	int			ret;
	t_vertex3d	c;

	fd = open(filename, O_RDONLY);
	if (1 != (ret = ft_get_params_map(fd, &width, &len)))
		return (ret == -1 ? ENOENT : EIO);
	if (!(mlx->srcmap = init_map(width, len)))
		return (ENOMEM);
	close(fd);
	fd = open(filename, O_RDONLY);
	if (!(ft_parse_file(fd, mlx)))
		return (EIO);
	c = mlx->srcmap->vrtxs[mlx->srcmap->height - 1][mlx->srcmap->width - 1];
	translate_map(mlx->srcmap, -c.x / 2.0, -c.y / 2.0, 0.0);
	move_center(mlx->srcmap, mlx);
	if (!(mlx->map = ft_copy_map(mlx->srcmap)))
		return (ENOMEM);
	return (1);
}

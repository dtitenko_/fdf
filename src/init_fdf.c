/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_fdf.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 14:04:02 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:53:15 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

t_vertex3d	*init_vertex3d(void)
{
	t_vertex3d *v_p;

	if (!(v_p = (t_vertex3d *)malloc(sizeof(t_vertex3d))))
		return (NULL);
	v_p->x = 0;
	v_p->y = 0;
	v_p->z = 0;
	v_p->color.color = 0x00FFFFFF;
	return (v_p);
}

t_map		*init_map(int w, int h)
{
	int		i;
	t_map	*map;

	if (!(map = (t_map *)malloc(sizeof(t_map))))
		return (NULL);
	map->height = h;
	map->width = w;
	map->vrtxs = NULL;
	if (!(map->vrtxs = (t_vertex3d **)malloc(h * sizeof(t_vertex3d *))))
	{
		ft_mapdel(&map);
		return (NULL);
	}
	i = -1;
	while (++i < h)
	{
		map->vrtxs[i] = NULL;
		if (!(map->vrtxs[i] = (t_vertex3d *)malloc(w * sizeof(t_vertex3d))))
		{
			ft_mapdel(&map);
			return (NULL);
		}
	}
	return (map);
}

t_trans		*init_trans(void)
{
	t_trans	*trans;

	if (!(trans = (t_trans *)malloc(sizeof(t_trans))))
		return (NULL);
	ft_transzero(trans);
	return (trans);
}

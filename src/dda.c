/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dda.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 15:55:53 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:56:19 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <math.h>

t_vertex3d	ft_diff(t_vertex3d v1, t_vertex3d v2, int *steps)
{
	t_vertex3d	d;

	d.x = (v2.x - v1.x);
	d.y = (v2.y - v1.y);
	if (v2.color.color > v1.color.color)
	{
		d.color.argb.r = v2.color.argb.r - v1.color.argb.r;
		d.color.argb.b = v2.color.argb.g - v1.color.argb.g;
		d.color.argb.g = v2.color.argb.b - v1.color.argb.b;
	}
	else
	{
		d.color.argb.r = v1.color.argb.r - v2.color.argb.r;
		d.color.argb.b = v1.color.argb.g - v2.color.argb.g;
		d.color.argb.g = v1.color.argb.b - v2.color.argb.b;
	}
	(*steps) = (int)((fabs(d.x) >= fabs(d.y)) ? fabs(d.x) : fabs(d.y));
	d.x = (*steps) ? d.x / *steps : 0.0;
	d.y = (*steps) ? d.y / *steps : 0.0;
	return (d);
}

int			ft_color_perc(t_color color, double perc)
{
	t_color tmp;

	tmp.argb.r = (unsigned char)(color.argb.r * perc);
	tmp.argb.g = (unsigned char)(color.argb.g * perc);
	tmp.argb.b = (unsigned char)(color.argb.b * perc);
	return (tmp.color);
}

void		ft_dda(t_vertex3d v1, t_vertex3d v2, t_mlx *mlx)
{
	double		i;
	int			pixel;
	t_vertex3d	d;
	t_vertex3d	tmp;

	d = ft_diff(v1, v2, &pixel);
	tmp.x = v1.x;
	tmp.y = v1.y;
	tmp.color.color = v1.color.color;
	i = 0;
	while (i++ <= pixel)
	{
		img_put_pixel(mlx->img_p, &tmp, tmp.color.color);
		tmp.x += d.x;
		tmp.y += d.y;
		if (v1.color.color < v2.color.color)
			tmp.color.color = v1.color.color +
					ft_color_perc(d.color, i / pixel);
		else if (v1.color.color > v2.color.color)
			tmp.color.color = v1.color.color -
							ft_color_perc(d.color, i / pixel);
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   linalg_init.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 14:04:02 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:53:22 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "linalg.h"

t_vector4d	*init_vector4d(void)
{
	t_vector4d	*vec;
	int			i;

	if (!(vec = (t_vector4d *)malloc(sizeof(t_vector4d))))
		return (NULL);
	i = -1;
	while (++i < 4)
		(*vec)[i] = 0;
	return (vec);
}

t_matrix4x4	*init_matrix4x4(void)
{
	t_matrix4x4	*m;
	int			i;
	int			j;

	if (!(m = (t_matrix4x4 *)malloc(sizeof(t_matrix4x4))))
		return (NULL);
	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
			(*m)[i][j] = 0;
	}
	return (m);
}

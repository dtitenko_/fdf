/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/16 15:05:57 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/16 15:56:48 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <stdio.h>
#include "fdf.h"

int		keypress_hooks(int keycode, t_mlx *mlx)
{
	if (XK_Escape == keycode)
		exit_x(mlx);
	ft_modification_handlers(mlx, keycode);
	ft_trans_handler(mlx, keycode);
	ft_rotate_handler(mlx, keycode);
	ft_scale_handler(mlx, keycode);
	update(mlx);
	return (0);
}

int		keyrelease_hooks(int keycode, t_mlx *mlx)
{
	if (XK_Alt_L == keycode || XK_Alt_R == keycode)
		mlx->keymodif &= ~ALTMOD;
	else if (XK_Shift_L == keycode || XK_Shift_R == keycode)
		mlx->keymodif &= ~SHFTMOD;
	else if (XK_Control_L == keycode || XK_Control_R == keycode)
		mlx->keymodif &= ~CTRLMOD;
	ft_draw_handlers(mlx, keycode);
	mlx->flags |= LINESMODE;
	update(mlx);
	return (0);
}

int		exit_x(t_mlx *mlx)
{
	mlx_destroy_image(mlx->mlx_p, mlx->img_p);
	mlx_destroy_window(mlx->mlx_p, mlx->win_p);
	exit(0);
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   projections.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 14:04:02 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:53:37 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "math.h"
#include "linalg.h"
#include "affine.h"

t_vertex3d	cabinet(t_vertex3d v)
{
	double	angl;

	angl = 135. * M_PI / 180;
	v.x = v.x + 1.0 / 2.0 * v.z * cos(angl);
	v.y = v.y + 1.0 / 2.0 * v.z * sin(angl);
	v.z = 0.0;
	return (v);
}

t_vertex3d	ortho(t_vertex3d v)
{
	v.z = 0;
	return (v);
}

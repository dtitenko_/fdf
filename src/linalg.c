/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   linalg.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 14:04:02 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:53:19 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "linalg.h"

t_vector4d	*vrtx2vec(t_vertex3d vrtx, t_vector4d *vec)
{
	(*vec)[0] = vrtx.x;
	(*vec)[1] = vrtx.y;
	(*vec)[2] = vrtx.z;
	(*vec)[3] = 1;
	return (vec);
}

t_vertex3d	vec2vrtx(t_vector4d vec, t_vertex3d vrtx)
{
	vrtx.x = vec[0] / vec[3];
	vrtx.y = vec[1] / vec[3];
	vrtx.z = vec[2] / vec[3];
	return (vrtx);
}

t_matrix4x4	*identity(t_matrix4x4 *m)
{
	(*m)[0][0] = 1;
	(*m)[0][1] = 0;
	(*m)[0][2] = 0;
	(*m)[0][3] = 0;
	(*m)[1][0] = 0;
	(*m)[1][1] = 1;
	(*m)[1][2] = 0;
	(*m)[1][3] = 0;
	(*m)[2][0] = 0;
	(*m)[2][1] = 0;
	(*m)[2][2] = 1;
	(*m)[2][3] = 0;
	(*m)[3][0] = 0;
	(*m)[3][1] = 0;
	(*m)[3][2] = 0;
	(*m)[3][3] = 1;
	return (m);
}

t_vertex3d	dot_mxv(t_vertex3d vrtx, t_matrix4x4 m)
{
	t_vector4d	vec;
	t_color		tmpcolor;
	t_vector4d	c;
	int			i;
	int			j;

	tmpcolor = vrtx.color;
	vrtx2vec(vrtx, &vec);
	i = -1;
	while (++i < 4)
	{
		j = -1;
		c[i] = 0;
		while (++j < 4)
			c[i] += vec[j] * m[i][j];
	}
	vrtx = vec2vrtx(c, vrtx);
	vrtx.color = tmpcolor;
	return (vrtx);
}

t_matrix4x4	*dot_mxm(t_matrix4x4 m1, t_matrix4x4 m2, t_matrix4x4 *out)
{
	int			i;
	int			j;
	int			r;

	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			r = -1;
			(*out)[i][j] = 0;
			while (++r < 4)
				(*out)[i][j] += m1[i][r] * m2[r][j];
		}
	}
	return (out);
}

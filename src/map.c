/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 16:08:26 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:53:33 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_map	*ft_copy_map(t_map *srcmap)
{
	t_map	*map;
	int		i;
	int		j;

	if (!(map = init_map(srcmap->width, srcmap->height)))
		return (NULL);
	i = -1;
	while (++i < srcmap->height)
	{
		j = -1;
		while (++j < srcmap->width)
			map->vrtxs[i][j] = srcmap->vrtxs[i][j];
	}
	return (map);
}

t_map	*translate_map(t_map *map, double tx, double ty, double tz)
{
	t_trans	trans;

	ft_transzero(&trans);
	trans.tx = tx;
	trans.ty = ty;
	trans.tz = tz;
	transform_map(map, &trans);
	return (map);
}

t_map	*rotate_map(t_map *map, double rx, double ry, double rz)
{
	t_trans	trans;

	ft_transzero(&trans);
	trans.rotx = rx;
	trans.roty = ry;
	trans.rotz = rz;
	transform_map(map, &trans);
	return (map);
}

t_map	*scale_map(t_map *map, double sx, double sy, double sz)
{
	t_trans	trans;

	ft_transzero(&trans);
	trans.sx = (sx <= 0) ? 1 : sx;
	trans.sy = (sy <= 0) ? 1 : sy;
	trans.sz = (sz <= 0) ? 1 : sz;
	transform_map(map, &trans);
	return (map);
}

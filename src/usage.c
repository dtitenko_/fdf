/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 15:08:50 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:53:47 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <libft.h>
#include "fdf.h"

static void	put_string(t_mlx *mlx, int x, int y, char *str)
{
	mlx_string_put(mlx->mlx_p, mlx->win_p, x, y, 0xFFFFFF, str);
}

void		draw_usage(t_mlx *mlx)
{
	put_string(mlx, 10, 15, "Usage:");
	put_string(mlx, 10, 30, "F1 - Show/Hide this usage;");
	put_string(mlx, 10, 45, "F2 - Show/Hide this info about transformations;");
	put_string(mlx, 10, 60, "F3 - Show/Hide axis;");
	put_string(mlx, 10, 75, "q, w, e, a, s, d - rotate figure;");
	put_string(mlx, 10, 90, "z, x, c - scale up(down, if Ctrl is pressed)");
	put_string(mlx, 70, 105, "z, x, y, respectively;");
	put_string(mlx, 10, 120, "+/-(Numpad) - scale up/down whole figure;");
	put_string(mlx, 10, 135, "arrows - translate figure;");
	put_string(mlx, 10, 150, "l - enable/disable diagonal lines;");
	put_string(mlx, 10, 165, "p - change projection type;");
	put_string(mlx, 10, 180, "f - draw with quads/lines;");
	put_string(mlx, 10, 195, "r - reset all transformations.");
}

static void	fill_table(t_mlx *mlx, int x, int y)
{
	char	*str[3][3];
	int		i;
	int		j;

	str[0][0] = ft_ftoa(mlx->trans->rotx, 1, 1);
	str[0][1] = ft_ftoa(mlx->trans->roty, 1, 1);
	str[0][2] = ft_ftoa(mlx->trans->rotz, 1, 1);
	str[1][0] = ft_ftoa(mlx->trans->tx, 1, 1);
	str[1][1] = ft_ftoa(mlx->trans->ty, 1, 1);
	str[1][2] = ft_ftoa(mlx->trans->tz, 1, 1);
	str[2][0] = ft_ftoa(mlx->trans->sx, 1, 1);
	str[2][1] = ft_ftoa(mlx->trans->sy, 1, 1);
	str[2][2] = ft_ftoa(mlx->trans->sz, 1, 1);
	i = -1;
	while (++i < 3)
	{
		j = -1;
		while (++j < 3)
			put_string(mlx, x + i * 100, y + j * 15, str[j][i]);
	}
}

void		draw_info(t_mlx *mlx)
{
	int x;
	int y;

	x = 100;
	y = 675;
	put_string(mlx, 10, y, "rotation");
	put_string(mlx, 10, y + 15, "translation");
	put_string(mlx, 10, y + 30, "scale");
	put_string(mlx, x, y - 15, "X");
	put_string(mlx, x + 100, y - 15, "Y");
	put_string(mlx, x + 200, y - 15, "Z");
	fill_table(mlx, x, y);
}

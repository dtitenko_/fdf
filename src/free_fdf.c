/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_fdf.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 15:51:21 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:51:33 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "fdf.h"

void	ft_mapdel(t_map **map)
{
	int i;

	if (!map || !(*map))
		return ;
	i = -1;
	while (++i < (*map)->height)
		if ((*map)->vrtxs && (*map)->vrtxs[i])
			ft_memdel((void **)&((*map)->vrtxs[i]));
	if ((*map)->vrtxs)
		ft_memdel((void **)(&((*map)->vrtxs)));
	ft_memdel((void **)map);
}

void	ft_mlxdel(t_mlx **mlx)
{
	if (*mlx)
	{
		mlx_destroy_image((*mlx)->mlx_p, (*mlx)->img_p);
		mlx_destroy_window((*mlx)->mlx_p, (*mlx)->win_p);
		ft_mapdel(&((*mlx)->srcmap));
		ft_mapdel(&((*mlx)->map));
		ft_memdel((void **)(&((*mlx)->trans)));
		ft_memdel((void **)mlx);
	}
}

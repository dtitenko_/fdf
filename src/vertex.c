/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vertex.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 19:02:00 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 15:53:51 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "affine.h"

t_vertex3d	transform_vertex(t_vertex3d v, t_trans *trans)
{
	if (trans->sx > 1 || trans->sz > 1 || trans->sy > 1)
		v = scale(v, trans->sx, trans->sy, trans->sz);
	if (trans->rotx != 0 || trans->roty != 0 || trans->rotz != 0)
		v = rotate(v, trans->rotx, trans->roty, trans->rotz);
	v = translate(v, trans->tx, trans->ty, trans->tz);
	return (v);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   affine.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 14:04:01 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 16:59:01 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_AFFINE_H
# define FDF_AFFINE_H

# include "linalg.h"

t_matrix4x4	*translate_matrix(t_matrix4x4 *m, double dx, double dy, double dz);
t_matrix4x4	*rotatex_matrix(t_matrix4x4 *m, double teta);
t_matrix4x4	*rotatey_matrix(t_matrix4x4 *m, double teta);
t_matrix4x4	*rotatez_matrix(t_matrix4x4 *m, double teta);
t_matrix4x4	*scale_matrix(t_matrix4x4 *m, double sx, double sy, double sz);

#endif

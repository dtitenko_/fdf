/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 15:08:50 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 16:59:59 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# define WIN_W 1024
# define WIN_H 710

# ifndef MLX_INT_H
#  define MLX_INT_H
#  include "mlx_int.h"
# endif
# include "mlx.h"
# include "ft_colors.h"

# define CABINETPROJ	0x01
# define ORTHOPROJ		0x02
# define POINTSMODE		0x04
# define LINESMODE		0x08
# define DIAGLINES		0x10
# define QUADSMODE		0x20
# define SHOWHELP		0x40
# define SHOWINFO		0x80
# define SHOWAXIS		0x100

/*
** modification keys flags
*/
# define CTRLMOD		0x01
# define SHFTMOD		0x02
# define ALTMOD			0x04

# include <errno.h>

typedef struct	s_vertex
{
	double		x;
	double		y;
	double		z;
	t_color		color;
}				t_vertex3d;

typedef struct	s_map
{
	t_vertex3d	**vrtxs;
	int			width;
	int			height;
}				t_map;

typedef struct	s_trans
{
	double		sx;
	double		sy;
	double		sz;
	double		tx;
	double		ty;
	double		tz;
	double		rotx;
	double		roty;
	double		rotz;
}				t_trans;

typedef struct	s_mlx
{
	t_xvar		*mlx_p;
	t_win_list	*win_p;
	t_img		*img_p;
	t_trans		*trans;
	t_map		*map;
	t_map		*srcmap;
	long		flags;
	int			keymodif;
	t_vertex3d	(*proj_func)();
}				t_mlx;

void			ft_die(int argc, char **argv, int code, t_mlx **mlx);
void			ft_linesdel(char ***lines);
void			move_center(t_map *map, t_mlx *mlx);
int				update(t_mlx *mlx);

t_vertex3d		*init_vertex3d();
t_map			*init_map(int w, int h);
t_trans			*init_trans();

int				ft_read(char *filename, t_mlx *mlx);

t_map			*ft_copy_map(t_map *srcmap);

void			ft_mapdel(t_map **map);
void			ft_mlxdel(t_mlx **mlx);

int				keypress_hooks(int keycode, t_mlx *mlx);
int				keyrelease_hooks(int keycode, t_mlx *mlx);
int				exit_x(t_mlx *mlx);

void			img_put_pixel(t_img *image, t_vertex3d *v, int color);
void			ft_dda(t_vertex3d v1, t_vertex3d v2, t_mlx *mlx);

t_vertex3d		cabinet(t_vertex3d v);
t_vertex3d		ortho(t_vertex3d v);

t_vertex3d		translate(t_vertex3d v, double x, double y, double z);
t_vertex3d		scale(t_vertex3d v, double sx, double sy, double sz);
t_vertex3d		rotate(t_vertex3d v, double rx, double ry, double rz);

t_map			*transform_map(t_map *map, t_trans *trans);
t_map			*translate_map(t_map *map, double tx, double ty, double tz);
t_map			*scale_map(t_map *map, double sx, double sy, double sz);
t_map			*rotate_map(t_map *map, double rx, double ry, double rz);

t_vertex3d		transform_vertex(t_vertex3d v, t_trans *trans);

void			ft_transzero(t_trans *trans);

void			draw_axis(t_mlx *mlx);
void			draw(t_mlx *mlx);
void			draw_points(t_mlx *mlx);

void			ft_scale_handler(t_mlx *mlx, int keycode);
void			ft_scale(t_trans *trans, double sx, double sy, double sz);

double			normalize_angle(double angl);
void			ft_rotate_handler(t_mlx *mlx, int keycode);
void			ft_trans_handler(t_mlx *mlx, int keycode);
void			ft_modification_handlers(t_mlx *mlx, int keycode);
void			ft_draw_handlers(t_mlx *mlx, int keycode);
void			draw_usage(t_mlx *mlx);
void			draw_info(t_mlx *mlx);
void			draw_axis(t_mlx *mlx);

#endif

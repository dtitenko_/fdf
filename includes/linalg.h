/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   linalg.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 14:04:01 by dtitenko          #+#    #+#             */
/*   Updated: 2017/05/09 16:59:53 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_LINALG_H
# define FDF_LINALG_H

# include "fdf.h"

typedef double	t_matrix4x4[4][4];
typedef double	t_vector4d[4];

t_vector4d		*init_vector4d();
t_matrix4x4		*init_matrix4x4();
t_matrix4x4		*identity(t_matrix4x4 *m);
t_vertex3d		vec2vrtx(t_vector4d vec, t_vertex3d vrtx);
t_vector4d		*vrtx2vec(t_vertex3d vrtx, t_vector4d *vec);
t_vertex3d		dot_mxv(t_vertex3d vrtx, t_matrix4x4 m);
t_matrix4x4		*dot_mxm(t_matrix4x4 m1, t_matrix4x4 m2, t_matrix4x4 *out);
t_matrix4x4		*mtrxcpy(t_matrix4x4 *dest, t_matrix4x4 *src);

#endif
